import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:task1/screens/start.dart';
import 'database/database_init.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Start(),
    );
  }
}
