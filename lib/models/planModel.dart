import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';

class Plan {
  int planID;
  int timeStamp;
  var createdAt;
  int quantityType;
  int quantity;
  int fromAya;
  int toAya;
  int startDate;
  int endDate;

  int endID; // save the last index note please replace it by 4 for each one

  Plan(
      {@required this.createdAt,
      @required this.endDate,
      @required this.fromAya,
      @required this.planID,
      @required this.quantity,
      @required this.quantityType,
      @required this.startDate,
      @required this.timeStamp,
      @required this.toAya,
      @required this.endID});
}
