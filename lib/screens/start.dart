import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:task1/database/database_init.dart';
import 'package:task1/models/planModel.dart';

class Start extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Start();
  }
}

class _Start extends State<Start> {
  void initState() {
    //getData(1);
    super.initState();
  }

  takeCommand(int endID, String type) {
    String command =
        'select * from (Aya )where id in( ( select min (id) from Aya where $type=${endID + 1} ) ,( select min (id) from Aya where $type=${endID - 1} ),( select max (id) from Aya where $type=${endID - 1} ),( select max (id) from Aya where $type=${endID + 1} ) )';
    return command;
  }

  getData(Plan p) async {
    Database dba = await DatabaseIntialization.db;
    switch (p.quantityType) {
      //juz
      case 1:
        dba.rawQuery(takeCommand(p.endID, 'juz'));
        break;
      case 2:
        //quarter
        dba.rawQuery(takeCommand(p.endID, 'quarter'));
        break;
      case 3:
        //Hezb
        dba.rawQuery(takeCommand(p.endID, 'Hezb'));
        break;
      case 4:
        //page
        print(dba.rawQuery(takeCommand(p.endID, 'page')).then((f) {
          f.forEach((k) {
            print(k);
          });
        }));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('New Hetma'),
      ),
      body: ListView(
        children: <Widget>[
          Text('Hetma Name'),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            child: TextField(
              decoration:
                  InputDecoration(hintText: 'Name', border: InputBorder.none),
            ),
          ),
          RaisedButton(
            onPressed: () {
              Plan p = new Plan(
                  planID: 1,
                  createdAt: 1,
                  endDate: 1,
                  fromAya: 1,
                  quantity: 1,
                  quantityType: 4,
                  startDate: 1,
                  timeStamp: 1,
                  toAya: 1,
                  endID: 1);
              getData(p);
            },
          )
        ],
      ),
    );
  }
}
