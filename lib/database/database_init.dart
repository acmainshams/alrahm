import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DatabaseIntialization {
  static Future<Database> get db async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "mushaf_data");
    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      await _loadDataBaseFormAssets(path);
    }

    return await openDatabase(
      path,
    );
  }

  static _loadDataBaseFormAssets(path) async {
    ByteData data = await rootBundle.load(join('assets', "mushaf_data.db"));
    List<int> bytes =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await new File(path).writeAsBytes(bytes);
  }
}
